import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyCRSpNiTGbOEgRf1S3lQrvOIvvl8KmA4w0",
    authDomain: "crwn-db-bad2d.firebaseapp.com",
    databaseURL: "https://crwn-db-bad2d.firebaseio.com",
    projectId: "crwn-db-bad2d",
    storageBucket: "crwn-db-bad2d.appspot.com",
    messagingSenderId: "28616221478",
    appId: "1:28616221478:web:9ae57fe255609b7ec917e7",
    measurementId: "G-BT92ERN6VE"
  };

  export const createUserProfileDocument = async(userAuth, additionalData) => {
    if(!userAuth) return;

    const userRef = firestore.doc(`users/${userAuth.uid}`);

    const snapShot = await userRef.get();

    if(!snapShot.exists) {
      const { displayName, email } = userAuth;
      const createdAt = new Date();

      try {
        await userRef.set({
          displayName,
          email,
          createdAt,
          ...additionalData
        })
      } catch(error) {
        console.log('Error creating user', error.message);
      }
    }

    return userRef;

  };

  firebase.initializeApp(config);

  export const auth = firebase.auth();
  export const firestore = firebase.firestore();

  const provider = new firebase.auth.GoogleAuthProvider();
  provider.setCustomParameters({ prompt: 'select_account' });
  export const signInWithGoogle = () => auth.signInWithPopup(provider);

  export default firebase;
